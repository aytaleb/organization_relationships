# README #

### What is this repository for? ###
Node js application to solve the following:

The goal of this task it to create a RESTful service that stores organisations with relations (parent to child relation). Organization name is unique. One organisation may have multiple parents and daughters. All relations and organisations are inserted with one request (endpoint 1).  API has a feature to retrieve all relations of one organization (endpoint 2). This endpoint response includes all parents, daughters and sisters of a given organization. Good luck!

Additional requirements: 
* Accepted programming languages: PHP, Node.js 
* Database: MySQL, SQLite or PostgreSQL 
 
 
The service endpoints: 
 
* REST API endpoint that would allow to add many organization with relations in one POST request: 
`
{  
   "org_name":"Paradise Island",
   "daughters":[  
      {  
         "org_name":"Banana tree",
         "daughters":[  
            {  
               "org_name":"Yellow Banana"
            },
            {  
               "org_name":"Brown Banana"
            },
            {  
               "org_name":"Black Banana"
            }
         ]
      },
      {  
         "org_name":"Big banana tree",
         "daughters":[  
            {  
               "org_name":"Yellow Banana"
            },
            {  
               "org_name":"Brown Banana"
            },
            {  
               "org_name":"Green Banana"
            },
            {  
               "org_name":"Black Banana",
               "daughters":[  
                  {  
                     "org_name":"Phoneutria Spider"
                  }
               ]
            }
         ]
      }
   ]
}
`
 * REST API endpoint that returns relations of one organization (queried by name). All organization daughters, sisters and parents are returned as one list. List is ​ordered by name​ and one page may ​return 100 rows​ at max with pagination support. For example if you query relations for organization “Black Banana”, you will get: 
 `
 [  
   {  
      "relationship_type":"parent",
      "org_name":"Banana tree"
   },
   {  
      "relationship_type":"parent",
      "org_name":"Big banana tree"
   },
   {  
      "relationship_type":"sister",
      "org_name":"Brown Banana"
   },
   {  
      "relationship_type":"sister",
      "org_name":"Green Banana"
   },
   {  
      "relationship_type":"daughter",
      "org_name":"Phoneutria Spider"
   },
   {  
      "relationship_type":"sister",
      "org_name":"Yellow Banana"
   }
]
 `

### How do I get set up? ###
## Installation

* `npm install`
* `createuser -U postgres -P -s -e pipedrive`
* `createdb -U pipedrive orgs-dev`
* `sequelize db:migrate`

## Run the solution
* `npm run dev`

## endpoints the solution
* 1) /api/organizations/bulkcreate 
* 2) /api/organizations/:org_name/relations/:page

