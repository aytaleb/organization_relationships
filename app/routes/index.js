const organizationsController = require('../controllers').organizations;
const organizationrelationsController = require('../controllers').organizationrelations;

module.exports = (app) => {
  app.get('/api', (req, res) => res.status(200).send({
    message: 'Welcome to the Organizations API!',
  }));

  app.post('/api/organizations', organizationsController.create);
  app.post('/api/organizations/bulkcreate', organizationsController.bulkCreate);
  app.get('/api/organizations', organizationsController.list);
  app.post('/api/organizations/:organizationId/relations', organizationrelationsController.create);
  app.get('/api/organizations/:org_name/relations/:page', organizationrelationsController.listRelationsByName);
  




};