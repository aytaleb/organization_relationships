const organizations = require('./organizations');
const organizationrelations = require('./organizationrelations');

module.exports = {
    organizations,
    organizationrelations,
};