const Organization = require('../models').Organization;
const OrganizationService = require('../services').organization;


module.exports = {
  create(req, res) {
    return Organization
      .create({
        org_name: req.body.org_name,
      })
      .then(organization => res.status(201).send(organization))
      .catch(error => res.status(400).send(error));
  },
  list(req, res) {
    return Organization
      .all()
      .then(organizations => res.status(200).send(organizations))
      .catch(error => res.status(400).send(error));
  },
  bulkCreate(req, res) {
    return OrganizationService
      .bulkCreate(req, res)
      .then(() => {
        res.status(200).send("completed")
      })
      .catch(error => res.status(200).send(error));
    ;
  }

};