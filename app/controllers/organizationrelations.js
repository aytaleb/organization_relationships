const OrganizationRelation = require('../models').OrganizationRelation;
const Organization = require('../models').Organization;

module.exports = {
    create(req, res) {
        return OrganizationRelation
            .create({
                relationship_type: req.body.relationship_type,
                org_name: req.body.org_name,
                organizationId: req.params.organizationId,
            })
            .then(organizationRelation => res.status(201).send(organizationRelation))
            .catch(error => res.status(400).send(error));
    },

    listRelationsByName(req, res) {
        console.log(req.params.org_name)
        return Organization
            .findOne({ where: { org_name: req.params.org_name } })
            .then(organization => {
                if (!organization) {
                    return res.status(404).send({
                        message: 'Organization Not Found',
                    });
                }
                let limit = 100;   // number of records per page
                let offset = 0;
                OrganizationRelation
                    .findAndCountAll({ where: { organizationId: organization.id } })
                    .then(data => {
                        let page = req.params.page;      // page number
                        let pages = Math.ceil(data.count / limit);
                        offset = limit * (page - 1);
                        OrganizationRelation
                            .findAll({
                                where: { organizationId: organization.id },
                                limit: limit,
                                offset: offset,
                                order: [
                                    ['org_name', 'ASC'],
                                ],
                                attributes: { exclude: ['id', 'createdAt', 'updatedAt', 'organizationId'] }

                            })
                            .then((organizationRelations) => {
                                res.status(200).json({ 'result': organizationRelations, 'count': data.count, 'pages': pages });
                            });
                    })

            })
            .catch(error => res.status(400).send(error));
    },
};