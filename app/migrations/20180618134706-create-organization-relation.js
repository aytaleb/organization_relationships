'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.createTable('OrganizationRelations', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: Sequelize.INTEGER
    },
    relationship_type: {
      type: Sequelize.STRING,
      allowNull: false
    },
    org_name: {
      type: Sequelize.STRING,
      allowNull: false
    },
    createdAt: {
      allowNull: false,
      type: Sequelize.DATE
    },
    updatedAt: {
      allowNull: false,
      type: Sequelize.DATE
    },
    organizationId: {
      type: Sequelize.INTEGER,
      onDelete: 'CASCADE',
      references: {
        model: 'Organizations',
        key: 'id',
        as: 'organizationId',
      },
    },


  }),

  down: (queryInterface /* , Sequelize */) => queryInterface.dropTable('OrganizationRelations'),

};