module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.createTable('Organizations', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      org_name: {
        type: Sequelize.STRING,
        allowNull: false,  
        unique: true
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    }),
  down: (queryInterface /*, Sequelize */) => queryInterface.dropTable('Organizations'),

};