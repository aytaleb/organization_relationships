module.exports = (sequelize, DataTypes) => {
  const OrganizationRelation = sequelize.define('OrganizationRelation', {
    relationship_type: { 
      type: DataTypes.STRING, 
      allowNull: false, 
    },
    org_name: {
      type: DataTypes.STRING, 
      allowNull: false
    },
  });
  OrganizationRelation.associate = (models) => {
    // associations can be defined here
    OrganizationRelation.belongsTo(models.Organization, {
      foreignKey: 'organizationId',
      onDelete: 'CASCADE',
    });
  };
  return OrganizationRelation;
};