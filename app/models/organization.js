module.exports = (sequelize, DataTypes) => {
  const Organization = sequelize.define('Organization', {
    org_name: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true,
    },
  });
  Organization.associate = (models) => {
    // associations can be defined here
    Organization.hasMany(models.OrganizationRelation, {
      foreignKey: 'organizationId',
      as: 'organizationRelations',
    });
  };
  return Organization;
};