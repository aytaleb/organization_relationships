const Organization = require('../models').Organization;
const OrganizationRelation = require('../models').OrganizationRelation;


module.exports = {
    bulkCreate(req) {
        return bulkCreateJson(req.body);
    }
};

function bulkCreateJson(org_json) {
    org_name = org_json.org_name;
    return Organization
        .findOrCreate({ where: { org_name: org_name } })
        .spread(parentOrg => {
            if (org_json.hasOwnProperty('daughters')) {
                let daughters = org_json.daughters;
                let daughtersNames = daughters.map(daughter => daughter.org_name);
                daughters.forEach(daughter => {
                    Organization
                        .findOrCreate({ where: { org_name: daughter.org_name } })
                        .spread(daughterOrg => {
                            let parentId = parentOrg.id;
                            let daughterId = daughterOrg.id;
                            let parentName = parentOrg.org_name;
                            let daughterName = daughterOrg.org_name;
                            insertRelation(parentId, daughterName, 'daughter')
                                .then(() => {
                                    insertRelation(daughterId, parentName, 'parent')
                                })
                                .then(() => {
                                    daughtersNames.forEach(sister => {
                                        if (sister != daughterName) {
                                            insertRelation(daughterId, sister, 'sister');
                                        }
                                    })
                                });
                            // or this :D  (2)

                            // insertRelation(parentId, daughterName, 'daughter');
                            // insertRelation(daughterId, parentName, 'parent')
                            // daughtersNames.forEach(sister => {
                            //     if (sister != daughterName) {
                            //         insertRelation(daughterId, sister, 'sister');
                            //     }
                            // }
                            // )

                        })

                    bulkCreateJson(daughter)
                })
            }
        })
}

// Not used
function insertRelation_(org_id, org_name, relation) {
    OrganizationRelation
        .findOrCreate({
            where: {
                org_name: org_name,
                organizationId: org_id
            }, defaults: { relationship_type: relation }
        })
        .then(orgRe => {
            return orgRe
        })
        .catch(error => console.log(error));
}
// insert a relation function
function insertRelation(org_id, org_name, relation) {
    return new Promise(function (resolve, reject) {
        OrganizationRelation
            .findOrCreate({
                where: {
                    org_name: org_name,
                    organizationId: org_id
                }, defaults: { relationship_type: relation }
            })
            .spread(organizationRelation => {

                resolve(organizationRelation)
            })
            .catch(error => reject(error));
    })
}